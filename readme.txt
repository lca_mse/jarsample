@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: Sample1
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/

Samples regarding .jar files

PackageDemo uses demo\A, demo\B, and demo\tools\C
A,B,C gehoeren zu Package demo, C zu demo.tools

build0: alles wird lokal compiliert.
demo wird nach target1 gemoved.
Der classpath wird auf target1 gesetzt.
(da der classpath per default auf "." steht, wird das packet demo in .\demo gesucht und gefunden, ohne
irgendeinem zutun. Um zu zeigen, dass es auch woanders gefunden wird, wird es im build0 nach target1 gemoved,
sonst renamed)

build1: wie build0, aber aus demo\ wird ein .jar File erzeugt und das Classpath auf das .jar gesetzt.
Da sind aber auch die sourcen enthalten.

Build2: wie build1, aber nun wird in ein anderes target verzeichnis uebersetzt (-d) und nur mehr die binaries ins .jar gestellt.

Build3: wie build2, aber zusaetzlich wird auch noch ein Main (ABC.Java) ins .jar gestellt und via manifest File festgelegt, dass das
  main gestartet werden soll.

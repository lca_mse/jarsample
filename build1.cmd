@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: PackageDemo uebersetzen -> A,B,C werden mituebersetzt.
@REM ##              Alles in .jar archiv uebertragen.
@REM ##              ACHTUNG: Da werden die Sourcen mitgepackt!
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 13-01-07
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

del /S *.class *.jar
javac PackageDemo.java
jar cvf ms_jarSample1.jar demo
ren demo demo0
echo .
echo . Variante1
echo .
java -cp ms_jarSample1.jar;. PackageDemo
echo .
echo Variante2: geht nicht! (kein CP)
echo .
java -cp . PackageDemo
ren demo0 demo

endlocal

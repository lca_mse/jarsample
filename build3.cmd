@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: ALLES incl. MAIN im .jar, via MANIFEST File
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 13-01-07
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

set CLASSPATH=
del /S *.class *.jar
md target
javac demo\ABC.java -d target
cd target
jar cvfm ms_jarSample3.jar ..\manifest.txt demo
echo .
echo . alles im .jar
echo .
java -jar ms_jarSample3.jar
cd ..

endlocal

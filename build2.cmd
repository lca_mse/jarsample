@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      ::
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 13-01-07
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

set CLASSPATH=
del /S *.class *.jar
md target
javac PackageDemo.java -d target
cd target
jar cvf ms_jarSample2.jar demo
ren demo demo0
echo .
echo . Variante1
echo .
java -cp ms_jarSample2.jar;. PackageDemo
echo .
echo Variante2: geht nicht! (kein CP)
echo .
java PackageDemo

ren demo0 demo
cd ..

endlocal

@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: PackageDemo uebersetzen -> A,B,C werden mituebersetzt.
@REM ##              Alles via Classpath suchen
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 13-01-07
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

del /S *.class *.jar
javac PackageDemo.java
md target1
move demo target1
echo .
echo . Variante1
echo .
java -cp target1;. PackageDemo
echo .
echo Variante2: geht nicht! (kein CP)
echo .
java -cp . PackageDemo
move target1\demo .

endlocal
